package com.signiant.arquillian_test;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;



@RunWith(Arquillian.class)
public class GreeterTest {

    // @Inject
    // private Greeter greeter;

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class).addClasses(Greeter.class, Stateful.class).addAsManifestResource(EmptyAsset.INSTANCE,
                "beans.xml");
    }
    //
    private static final int LOOP_COUNT = 10000;
    private static List<LoopResults> loopResultList;

    @BeforeClass
    public static void before() {
        loopResultList = new ArrayList<>();
    }

    @SuppressWarnings("unchecked")
    @AfterClass
    public static void after() {
        List<LoopResults> localList = loopResultList;
        Collections.sort(localList);

        for (LoopResults loopResult : localList) {
            System.out.println(format("%s: average nanos: %d, average micros: %d", loopResult.context,
                    loopResult.iterationAverageNanos, loopResult.iterationAverageNanos / 1000));
        }
    }

    //
    // @Test
    // public void testUnmanaged() {
    // doLoopTest(LOOP_COUNT, "unmanaged", () -> {
    // Unmanaged<Greeter> unmanagedGreeter = new Unmanaged<>(Greeter.class);
    // UnmanagedInstance<Greeter> greeterInstance =
    // unmanagedGreeter.newInstance();
    // Greeter greeter =
    // greeterInstance.produce().inject().postConstruct().get();
    // greeter.createGreeting("Hello");
    // });
    // }
    //
    // @Test
    // public void testUnmanagedInstance() {
    // Unmanaged<Greeter> unmanagedGreeter = new Unmanaged<>(Greeter.class);
    // doLoopTest(LOOP_COUNT, "unmanaged instance", () -> {
    // UnmanagedInstance<Greeter> greeterInstance =
    // unmanagedGreeter.newInstance();
    // Greeter greeter =
    // greeterInstance.produce().inject().postConstruct().get();
    // greeter.createGreeting("Hello");
    // });
    // }
    //
    // @Test
    // public void testInjector() {
    // doLoopTest(LOOP_COUNT, "CDI current", () -> {
    // Greeter greeter = CDI.current().select(Greeter.class).get();
    // greeter.createGreeting("Hello!");
    // });
    // }
    //
    // @Test
    // public void unmanagedTesting() {
    // Unmanaged<Stateful> unmanagedStateful = new Unmanaged<>(Stateful.class);
    // Stateful s1 = createInstance(unmanagedStateful);
    // Stateful s2 = createInstance(unmanagedStateful);
    // s1.setState("s1");
    // s2.setState("s2");
    // System.out.println(format("%s %s", s1.getState(), s2.getState()));
    // }

    @Test
    public void unmanagedInjectorTesting() {
        loopResultList.add(doLoopTest(LOOP_COUNT, "UnmanagedInjector", () -> {
            Greeter greeter = UnmanagedInjector.INSTANCE.inject(Greeter.class);
            greeter.createGreeting("Hello!");
        }));
    }

    @Test
    public void cachecCdiTesting() {
        loopResultList.add(doLoopTest(LOOP_COUNT, "CachedCdiInjector", () -> {
            Greeter greeter = CachedCdiInjector.INSTANCE.inject(Greeter.class);
            greeter.createGreeting("Hello!");
        }));
    }

    @Test
    public void cdiTesting() {
        loopResultList.add(doLoopTest(LOOP_COUNT, "CdiInjector", () -> {
            Greeter greeter = CdiInjector.INSTANCE.inject(Greeter.class);
            greeter.createGreeting("Hello!");
        }));
    }

    @Test
    public void newTesting() {
        loopResultList.add(doLoopTest(LOOP_COUNT, "new", () -> {
            Greeter greeter = new Greeter();
            greeter.createGreeting("Hello!");
        }));
    }

    @Test
    public void providerTesting() {
        loopResultList.add(doLoopTest(LOOP_COUNT, "ProviderInjector", () -> {
            Greeter greeter = ProviderInjector.INSTANCE.inject(Greeter.class);
            greeter.createGreeting("Hello!");
        }));
    }

    @Test
    public void hotProviderTesting() {
        loopResultList.add(doLoopTest(LOOP_COUNT, "Hot ProviderInjector", () -> {
            Greeter greeter = ProviderInjector.INSTANCE.inject(Greeter.class);
            greeter.createGreeting("Hello!");
        }));
    }

    @Test
    public void statefulTest() {
        Stateful s1 = ProviderInjector.INSTANCE.inject(Stateful.class);
        Stateful s2 = ProviderInjector.INSTANCE.inject(Stateful.class);
        s1.setState("s1");
        s2.setState("s2");

        System.out.println(format("%s %s", s1.getState(), s2.getState()));
    }

    private LoopResults doLoopTest(int loopCount, String context, Runnable runnable) {
        return doLoopTest(loopCount, context, runnable, false);
    }

    private LoopResults doLoopTest(int loopCount, String context, Runnable runnable, boolean printEach) {
        long totalTime = 0;
        for (int i = 0; i < loopCount; i++) {
            long startTime = System.nanoTime();
            runnable.run();
            long iterationTime = System.nanoTime() - startTime;
            totalTime += iterationTime;

            if (printEach) {
                System.out.println(iterationTime);
            }
        }
        long average = totalTime / loopCount;
        // long averageMicros = average / 1000;
        // System.out.println(format("Average %s: nanos %d, micros %d", context,
        // average, averageMicros));
        return new LoopResults(context, average);
    }

    private static class LoopResults implements Comparable<LoopResults> {
        public String context;
        public long iterationAverageNanos;

        public LoopResults(String context, long iterationAverageNanos) {
            this.context = context;
            this.iterationAverageNanos = iterationAverageNanos;
        }

        @Override
        public String toString() {
            return "LoopResults [context=" + context + ", iterationAverageNanos=" + iterationAverageNanos + "]";
        }

        @Override
        public int compareTo(LoopResults otherResults) {

            long diff = iterationAverageNanos - otherResults.iterationAverageNanos;
            if (diff < 0) {
                return -1;
            } else if (diff > 0) {
                return 1;
            }
            return 0;
        }

    }
}