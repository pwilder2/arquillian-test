package com.signiant.arquillian_test;

import javax.enterprise.inject.spi.CDI;

public enum CachedCdiInjector {
    INSTANCE;

    private final CDI<Object> cdi;

    CachedCdiInjector() {
        cdi = CDI.current();
    }

    public <T> T inject(Class<T> clazz) {
        return cdi.select(clazz).get();
    }
}
