package com.signiant.arquillian_test;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.inject.spi.Unmanaged;

public enum UnmanagedInjector {
    INSTANCE;

    private Map<Class<?>, Unmanaged<?>> unmanagedMap;

    UnmanagedInjector() {
        unmanagedMap = new HashMap<>();
    }

    public <T> T inject(Class<T> clazz) {
        if (!unmanagedMap.containsKey(clazz)) {
            unmanagedMap.put(clazz, new Unmanaged<>(clazz));
        }

        @SuppressWarnings("unchecked")
        Unmanaged<T> unmanaged = (Unmanaged<T>) unmanagedMap.get(clazz);

        return unmanaged.newInstance().produce().inject().postConstruct().get();
    }
}
