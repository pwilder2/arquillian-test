package com.signiant.arquillian_test;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Provider;

public enum ProviderInjector {
    INSTANCE;

    private Map<Class<?>, Provider<?>> providerMap;

    ProviderInjector() {
        providerMap = new HashMap<>();
    }

    public <T> T inject(Class<T> clazz) {
        if (!providerMap.containsKey(clazz)) {
            providerMap.put(clazz, CDI.current().select(clazz));
        }

        @SuppressWarnings("unchecked")
        Provider<T> provider = (Provider<T>) providerMap.get(clazz);

        return provider.get();
    }
}
