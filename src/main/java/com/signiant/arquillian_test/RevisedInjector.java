package com.signiant.arquillian_test;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Provider;

public class RevisedInjector {
    enum ProviderInjector {
        INSTANCE;

        // Could use a cache here over a map.
        private Map<Class<?>, Provider<?>> providerMap;

        ProviderInjector() {
            providerMap = new HashMap<>();
        }

        public <T> T inject(Class<T> clazz) {
            if (!providerMap.containsKey(clazz)) {
                providerMap.put(clazz, CDI.current().select(clazz));
            }

            @SuppressWarnings("unchecked")
            Provider<T> provider = (Provider<T>) providerMap.get(clazz);

            return provider.get();
        }
    }

    /**
     * @deprecated Causes considerable performance degradation. Use annotation
     *             based injection instead if possible.
     */
    @Deprecated
    public static <T> T getInstance(Class<T> clazz) {
        try {
            return ProviderInjector.INSTANCE.inject(clazz);
        } catch (IllegalStateException ex) {
            // String logMessage = "Unable to create instance of class " + clazz
            // + ". CDI does not function in this context";

            // if (LOGGER.isTraceEnabled()) {
            // LOGGER.trace(logMessage, ex);
            // } else {
            // LOGGER.warn(logMessage);
            // }

            return null;
        }
    }
}
