package com.signiant.arquillian_test;

import javax.enterprise.inject.spi.CDI;

public enum CdiInjector {
    INSTANCE;

    public <T> T inject(Class<T> clazz) {
        return CDI.current().select(clazz).get();
    }
}
