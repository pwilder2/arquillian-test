package com.signiant.arquillian_test;

public class Stateful {

    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
